;(function($, window, document, undefined) {
	'use strict';

	main.alert = {
		wrapper: '.alert-main',
		template: '<div class="alert-main"><a href="#" class="close" title="Fechar alerta">x</a></div>',

		bindings: function() {
			$(document).on({
				click: function(e) {
					$(this).closest(main.alert.wrapper).fadeOut(100, function() {
						$(this).remove();
					});

					e.preventDefault();
				}
			}, main.alert.wrapper + ' .close');
		},

		init: function() {
			main.alert.bindings();
		}
	};
}(jQuery, this, this.document));
