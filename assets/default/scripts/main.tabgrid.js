;(function($, window, document, undefined) {
	'use strict';

	main.tabgrid = {
		wrapper: '.tabgrid',

		bindings: function() {
			var $wrapper = $(main.tabgrid.wrapper);

			$wrapper.find('.tabgrid-index li a').on({
				click: function(e) {
					var $target = $($(this).attr('href'));

					$(this).closest('ul').find('li').removeClass('active');
					$(this).closest('li').addClass('active');
					$(this).closest(main.tabgrid.wrapper).find('.tabgrid-content').hide();
					$target.show();

					e.preventDefault();
				}
			});
		},

		init: function() {
			var $wrapper = $(main.tabgrid.wrapper);

			if ($wrapper.length) {
				$wrapper.each(function() {
					var $active = $(this).find('.active:first-child');

					if ($active.length)
						$($active.find('a').attr('href')).show();
				});

				main.tabgrid.bindings();
			}
		}
	};
}(jQuery, this, this.document));
