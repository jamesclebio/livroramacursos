;(function($, window, document, undefined) {
	'use strict';

	main.buy = {
		buttons: function($form) {
			var	$button_submit = $form.find('[type="submit"]'),
				$link_submit = $form.find('.link-submit'),
				buy_free = ($('.table-buy [data-free]').length) ? true : false,
				labels = {
					button: 'Efetuar Pagamento',
					link: 'Efetuar Pagto',
					free: 'Confirmar Compra'
				};

			(buy_free) ? $button_submit.text(labels.free) : $button_submit.text(labels.button);
			$link_submit.text(labels.link);
		},

		payment: function($form) {
			var	$button_submit = $form.find('[type="submit"]'),
				$link_submit = $form.find('.link-submit'),
				url = $form.data('validator'),
				data = $form.serialize();

			function alertClear() {
				var $alert = $('.alert-main');

				if ($alert.length)
					$alert.remove();
			}

			function resultError() {
				var	template_alert = '<div class="alert-main alert-main-error"><a href="#" class="close" title="Fechar alerta">x</a><p><strong>Ops! Algo deu errado... :(</strong></p><p>Houve uma falha inesperada em nosso servidor. Tente novamente.</p></div>';

				$form.data('validate', 'error');

				main.buy.buttons($form);
				$button_submit.removeAttr('disabled');
				$link_submit.removeAttr('disabled');

				alertClear();
				$('.heading-3').after(template_alert);
				$('html, body').animate({
					scrollTop: $('.alert-main').offset().top - 20
				}, 0);
			}

			$.ajax({
				cache: false,
				type: 'POST',
				url: url,
				data: data,

				beforeSend: function() {
					alertClear();
					$button_submit.attr('disabled', 'disabled').text('Validando os dados...');
					$link_submit.attr('disabled', 'disabled').text('Validando...');
				},

				error: function() {
					resultError();
				},

				success: function(data) {
					switch (data) {
						case '0':
						case '':
							resultError();
							break;

						case 'gratis':
							document.location.href = '/compra/passo-3';
							break;

						default:
							$form.data('validate', 'success').append(data).submit();
							break;
					}
				}
			});
		},

		bindings: function() {
			var	$form = $('.form-pay');

			$form.on({
				submit: function(e) {
					if ($form.data('validate') != 'success') {
						main.buy.payment($(this));
						e.preventDefault();
					}
				}
			});

			$form.find('.link-submit').on({
				click: function(e) {
					$(this).closest('form').submit();
					e.preventDefault();
				}
			});
		},

		init: function() {
			var	$wrapper = $('.form-pay');

			if ($wrapper.length) {
				main.buy.buttons($wrapper);
				main.buy.bindings();
			}
		}
	};
}(jQuery, this, this.document));
