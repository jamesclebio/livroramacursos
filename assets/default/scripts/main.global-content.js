;(function($, window, document, undefined) {
	'use strict';

	main.globalContent = {
		scrollPage: function() {
			if (!$('html').hasClass('layout-home') && !$('html').hasClass('layout-buy')) {
				$('html, body').animate({
					scrollTop: $('.global-content').offset().top - 20
				}, 1000);
			}
		},

		init: function() {
			main.globalContent.scrollPage();
		}
	};
}(jQuery, this, this.document));
