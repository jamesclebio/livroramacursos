;(function($, window, document, undefined) {
	'use strict';

	main.social = {
		wrapper: '.global-footer .social .services',
		url: 'http://www.livroramacursos.com.br',
		description: 'Livrorama Cursos - Melhore sua vida. Conquiste seus objetivos.',

		twitter: function() {
			var	$wrapper = $(main.social.wrapper);

			$wrapper.append('<div class="twitter"><a href="https://twitter.com/share" class="twitter-share-button" data-url="' + main.social.url + '" data-text="' + main.social.description + '" data-lang="pt" data-hashtags="livroramacursos">Tweetar</a></div>');

			!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
		},

		googleplus: function() {
			var	$wrapper = $(main.social.wrapper);

			$wrapper.append('<div class="googleplus"><div class="g-plusone" data-size="medium" data-href="' + main.social.url + '"></div></div>');
			window.___gcfg = {lang: 'pt-BR'};

			(function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				po.src = 'https://apis.google.com/js/plusone.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			})();
		},

		facebook: function() {
			var	$wrapper = $(main.social.wrapper);

			$('body').prepend('<div id="fb-root"></div>');
			$wrapper.append('<div class="facebook"><div class="fb-like" data-href="' + main.social.url + '" data-layout="button_count" data-show-faces="false" data-send="false"></div></div>');

			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=239539972764108";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		},

		init: function() {
			var	$wrapper = $(main.social.wrapper);

			if ($wrapper.length) {
				main.social.twitter();
				main.social.googleplus();
				main.social.facebook();
			}
		}
	};
}(jQuery, this, this.document));
