;(function($, window, document, undefined) {
	'use strict';

	main.viewerCourses = {
		wrapper: '.viewer-courses',

		support: function() {
			var	$form = $('#form-support'),
				$input = $form.find('textarea'),
				$alert = $form.find('.alert'),
				url = $form.attr('action'),
				data = $form.serialize();

			function alertError() {
				$alert
					.addClass('alert-error')
					.html('<strong>Ops, não deu!</strong> Ocorreu algo inesperado em nosso servidor. Tente novamente.');
			}

			$.ajax({
				type: 'POST',
				url: url,
				data: data,

				beforeSend: function() {
					$alert
						.removeClass('alert-error')
						.addClass('alert-loading')
						.html('<strong>Enviando mensagem...</strong>')
						.show();
				},

				error: function() {
					alertError();
				},

				success: function(data) {
					if (data == 0)
						alertError()
					else if (data == 1) {
						$alert
							.addClass('alert-success')
							.html('<strong>Mensagem enviada!</strong> O especialista irá lhe responder por e-mail o mais breve possível.')
							.delay(10000)
							.fadeOut(300, function() {
								$(this).attr('class', 'alert');
							});

						$input.val('');
					}
				},

				complete: function() {
					$alert.removeClass('alert-loading');
				}
			});
		},

		bindings: function() {
			var $wrapper = $(main.viewerCourses.wrapper);

			$wrapper.find('.support form').on({
				submit: function(e) {
					main.viewerCourses.support();
					e.preventDefault();
				}
			});

			$wrapper.find('.list-courses .button-disabled').on({
				click: function(e) {
					e.preventDefault();
				}
			});
		},

		init: function() {
			var $wrapper = $(main.viewerCourses.wrapper);

			if ($wrapper.length) {
				main.viewerCourses.bindings();
			}
		}
	};
}(jQuery, this, this.document));
