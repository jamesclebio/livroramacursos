;(function($, window, document, undefined) {
	'use strict';

	main.passwordRemember = {
		wrapper: '#form-password-remember',

		bindings: function() {
			$(document).on({
				submit: function(e) {
					var	url = $(this).attr('action'),
						data = $(this).serialize(),
						$form = $(this),
						$button_submit = $(this).find('[type="submit"]'),
						$alert = $(this).find('.alert');

					function alertClear() {
						var $alert_main = $form.prev(main.alert.wrapper);

						if ($alert_main.length)
							$alert_main.remove();
					}

					function alertError() {
						$form
							.before(main.alert.template)
							.prev(main.alert.wrapper)
								.addClass('alert-main-error')
								.append('<p><strong>Ops! Algo deu errado... :(</strong></p><p>Houve uma falha inesperada em nosso servidor. Tente novamente.</p>');
					}

					$.ajax({
						type: 'POST',
						url: url,
						data: data,

						beforeSend: function() {
							$button_submit.attr('disabled', 'disabled');
							$alert
								.addClass('alert-loading')
								.html('<strong>Recuperando a senha...</strong>')
								.show();
						},

						error: function() {
							alertClear();						
							alertError();
						},

						success: function(data) {
							alertClear();

							if (data == 1) {
								$form
									.before(main.alert.template)
									.prev(main.alert.wrapper)
										.addClass('alert-main-success')
										.append('<p><strong>Senha enviada por e-mail!</strong></p><p>Verique o e-mail do usuário para visualizar a senha.</p>');
							} else if (data == 0) {
								$form
									.before(main.alert.template)
									.prev(main.alert.wrapper)
										.addClass('alert-main-error')
										.append('<p><strong>O usuário informado não existe!</strong></p><p>Tente novamente informando um outro nome de usuário.</p>');
							} else
								alertError();
						},

						complete: function() {
							$button_submit.removeAttr('disabled');
							$alert
								.removeClass('alert-loading')
								.empty()
								.hide();
						}
					});

					e.preventDefault();
				}
			}, main.passwordRemember.wrapper);
		},

		init: function() {
			main.passwordRemember.bindings();
		}
	};
}(jQuery, this, this.document));
