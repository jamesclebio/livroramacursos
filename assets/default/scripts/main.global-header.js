;(function($, window, document, undefined) {
	'use strict';

	main.globalHeader = {
		slider: function() {
			var $wrapper = $('.global-header .slider');

			if ($('html').hasClass('layout-home') && $wrapper.length) {
				$wrapper.after('<div class="slider-index" />').cycle({
					timeout: 3000,
					pager: '.slider-index'
				});
			}
		},

		login: function() {
			var	$form_login = $('#form-top-login'),
				$alert = $form_login.find('.alert'),
				url = $form_login.attr('action'),
				data = $form_login.serialize();

			function alertError() {
				$alert
					.removeClass('alert-loading')
					.html('<strong>Dados inválidos!</strong> Tente novamente.</div>');
			}

			$.ajax({
				type: 'POST',
				url: url,
				data: data,

				beforeSend: function() {
					$alert
						.addClass('alert-loading')
						.html('<strong>Acessando sua conta...</strong>')
						.show();
				},

				error: function() {
					alertError();
				},

				success: function(data) {
					(data == '') ? alertError() : document.location.href = data;
				}
			});
		},

		bindings: function() {
			var	$wrapper = $('.global-header');

			$wrapper.find('#form-top-login').on({
				submit: function(e) {
					main.globalHeader.login();
					e.preventDefault();
				}
			});

			$wrapper.find('.logged > ul > li > a').on({
				mouseenter: function() {
					var sub = $(this).next('ul');

					if (sub.length && !sub.is(':visible')) {
						$wrapper.find('ul ul').hide();
						$(this).addClass('active');
						sub.clearQueue().fadeIn(0);
					}
				},

				mouseleave: function() {
					var sub = $(this).next('ul');

					if (sub.is(':visible')) {
						sub.delay(10).fadeOut(0, function() {
							sub.prev('a').removeClass('active');
						});
					}
				}
			});

			$wrapper.find('.logged ul ul').on({
				mouseenter: function() {
					$(this).clearQueue();
				},

				mouseleave: function() {
					$(this).prev('a').removeClass('active');
					$(this).fadeOut(0);
				}
			});
		},

		init: function() {
			main.globalHeader.slider();
			main.globalHeader.bindings();
		}
	};
}(jQuery, this, this.document));
