;(function($, window, document, undefined) {
	'use strict';

	main.tabbar = {
		wrapper: '.tabbar',

		bindings: function() {
			var $wrapper = $(main.tabbar.wrapper);

			$wrapper.find('.tabbar-index li a').on({
				click: function(e) {
					var $target = $($(this).attr('href'));

					$(this).closest('ul').find('li').removeClass('active');
					$(this).closest('li').addClass('active');
					$(this).closest(main.tabbar.wrapper).find('.tabbar-content').hide();
					$target.show();

					e.preventDefault();
				}
			});
		},

		init: function() {
			var $wrapper = $(main.tabbar.wrapper);

			if ($wrapper.length) {
				$wrapper.each(function() {
					var $active = $(this).find('.active:first-child');

					if ($active.length)
						$($active.find('a').attr('href')).show();
				});

				main.tabbar.bindings();
			}
		}
	};
}(jQuery, this, this.document));
