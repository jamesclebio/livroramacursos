;(function($, window, document, undefined) {
	'use strict';

	main.splash = {
		wrapper: '.splash-main',

		build: function() {
			var $wrapper = $(main.splash.wrapper),
				template_close = '<a href="#" class="close">x</a>',
				timeout = 7000;

				if ($wrapper.data('timeout'))
					timeout = $wrapper.data('timeout');

				if ($wrapper.data('close'))
					$wrapper.append(template_close);

				$wrapper.fadeIn(300, function() {
					setTimeout(function() {
						main.splash.close();
					}, timeout);
				});
		},

		close: function() {
			var $wrapper = $(main.splash.wrapper);

			$wrapper.fadeOut(100, function() {
				$(this).remove();
			});
		},

		bindings: function() {
			var $wrapper = $(main.splash.wrapper);

			$wrapper.on({
				click: function(e) {
					var url = $(this).attr('href');

					if ($wrapper.data('blank'))
						window.open(url);
					else
						window.location.href = url;

					main.splash.close();
					e.preventDefault();
				}
			}, '.content a');

			$wrapper.on({
				click: function(e) {
					main.splash.close();
					e.preventDefault();
				}
			}, '.close');
		},

		init: function() {
			var $wrapper = $(main.splash.wrapper);

			if ($wrapper.length) {
				main.splash.build();
				main.splash.bindings();
			}
		}
	};
}(jQuery, this, this.document));
