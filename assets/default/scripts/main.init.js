;(function($, window, document, undefined) {
	'use strict';

	main.init = function() {
		var	plugins = [
			'globalHeader',
			'globalNav',
			'globalContent',
			'form',
			'table',
			'alert',
			'modal',
			'collapse',
			'passwordRemember',
			'tabbar',
			'tabgrid',
			'advantages',
			'viewerCourses',
			'buy',
			'video',
			'splash',
			// 'social'
		];

		if (arguments.length)
			plugins = arguments;

		for (var i in plugins)
			this[plugins[i]].init();
	};

	main.init();
}(jQuery, this, this.document));
