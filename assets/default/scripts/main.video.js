;(function($, window, document, undefined) {
	'use strict';

	main.video = {
		playlist: [],

		player: function() {
			var	$wrapper = $('#video-player-html');

			main.video.playlist = $wrapper.data('playlist').split('|');

			for (var i = 0; i < main.video.playlist.length; i++)
				main.video.playlist[i] = main.video.playlist[i].substr(0, main.video.playlist[i].lastIndexOf('=') + 1) + $.md5('livrorama' + main.video.playlist[i].substr(main.video.playlist[i].lastIndexOf('=') + 1));

			$wrapper
				.data('sequence', 0)
				.attr('src', main.video.playlist[0]);
		},

		bindings: function() {
			var	$wrapper = $('#video-player-html'),
				player = document.getElementById('video-player-html');

			player.addEventListener('ended', function() {
				$wrapper.data('sequence', $wrapper.data('sequence') + 1);

				if ($wrapper.data('sequence') < main.video.playlist.length) {
					player.setAttribute('src', main.video.playlist[$wrapper.data('sequence')]);
					player.load();
					player.play();
				}
			}, false);
		},

		init: function() {
			var	$wrapper = $('#video-player-html');

			if ($wrapper.length) {
				main.video.player();
				main.video.bindings();
			}
		}
	};
}(jQuery, this, this.document));
