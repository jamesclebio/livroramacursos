;(function($, window, document, undefined) {
	'use strict';

	main.advantages = {
		wrapper: '.advantages',

		init: function() {
			var $wrapper = $(main.advantages.wrapper);

			if ($wrapper.length) {
				$wrapper.find('.container').cycle({
					fx: 'fade',
					speed: 'fast',
					timeout: 4000,
					next: $wrapper.find('.next'),
					prev: $wrapper.find('.prev')
				});
			}
		}
	};
}(jQuery, this, this.document));
