;(function($, window, document, undefined) {
	'use strict';

	main.table = {
		sort: function() {
			var $table_cart = $('.table-sort');

			if ($table_cart.length) {
				$table_cart.tablesorter({
					dateFormat: 'uk'
				});
			}
		},

		init: function() {
			main.table.sort();
		}
	};
}(jQuery, this, this.document));
