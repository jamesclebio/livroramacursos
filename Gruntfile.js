module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		project: {
			banner:	'/* Built on <%= grunt.template.today("dddd, mmmm dS, yyyy, h:MM:ss TT") %> */',
			default: {
				scripts: {
					main: [
						'bower_components/swfobject/swfobject.js',
						'bower_components/jquery/dist/jquery.js',
						'bower_components/jquery.metadata/jquery.metadata.js',
						'bower_components/jquery-cycle/jquery.cycle.all.js',
						'bower_components/jquery-maskedinput/dist/jquery.maskedinput.js',
						'bower_components/jquery.tablesorter/js/jquery.tablesorter.js',
						'bower_components/chosen/public/chosen.jquery.js',
						'bower_components/jquery-md5/jquery.md5.js',
						'assets/default/scripts/main.core.js',
						'assets/default/scripts/main.global-header.js',
						'assets/default/scripts/main.global-nav.js',
						'assets/default/scripts/main.global-content.js',
						'assets/default/scripts/main.form.js',
						'assets/default/scripts/main.table.js',
						'assets/default/scripts/main.alert.js',
						'assets/default/scripts/main.modal.js',
						'assets/default/scripts/main.collapse.js',
						'assets/default/scripts/main.password-remember.js',
						'assets/default/scripts/main.tabbar.js',
						'assets/default/scripts/main.tabgrid.js',
						'assets/default/scripts/main.advantages.js',
						'assets/default/scripts/main.viewer-courses.js',
						'assets/default/scripts/main.buy.js',
						'assets/default/scripts/main.video.js',
						'assets/default/scripts/main.splash.js',
						'assets/default/scripts/main.social.js',
						'assets/default/scripts/main.init.js',
						'assets/default/scripts/main.js'
					],
					init: [
						'bower_components/modernizr/modernizr.js',
						'assets/default/scripts/init.js'
					]
				},
				styles: {
					path: 'assets/default/styles',
					compile: '<%= project.default.styles.path %>/**/*.sass'
				}
			}
		},

		jshint: {
			options: {
				force: true
			},
			default_main: ['assets/default/scripts/**/*', '!assets/default/scripts/init.js'],
			default_init: ['assets/default/scripts/init.js']
		},

		uglify: {
			options: {
				banner:	'<%= project.banner %>\n'
			},
			default_main: {
				src: '<%= project.default.scripts.main %>',
				dest: 'deploy/assets/default/scripts/main.js'
			},
			default_init: {
				src: '<%= project.default.scripts.init %>',
				dest: 'deploy/assets/default/scripts/init.js'
			}
		},

		compass: {
			options: {
				banner:	'<%= project.banner %>',
				relativeAssets: true,
				noLineComments: true,
				outputStyle: 'compressed',
				raw: 'preferred_syntax = :sass'
			},
			default: {
				options: {
					sassDir: '<%= project.default.styles.path %>',
					specify: '<%= project.default.styles.compile %>',
					cssDir: 'deploy/assets/default/styles',
					imagesDir: 'deploy/assets/default/images',
					javascriptsDir: 'deploy/assets/default/scripts',
					fontsDir: 'deploy/assets/default/fonts'
				}
			}
		},

		watch: {
			options: {
				livereload: true
			},
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['default']
			},
			default_scripts_main: {
				files: ['<%= project.default.scripts.main %>'],
				tasks: ['jshint:default_main', 'uglify:default_main']
			},
			default_scripts_init: {
				files: ['<%= project.default.scripts.init %>'],
				tasks: ['jshint:default_init', 'uglify:default_init']
			},
			default_styles: {
				files: ['<%= project.default.styles.compile %>'],
				tasks: ['compass:default']
			},
			app: {
				files: ['deploy/app/**/*']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['jshint', 'uglify', 'compass']);
};
