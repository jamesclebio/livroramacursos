<?php
class Teacher extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView(false);
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}

	public function all() {
		$this->setView('teacher-all');
	}

	public function profile() {
		$this->setView('teacher-profile');
	}
}
