<?php
class Search extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('search');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
