<?php
class Denied extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('denied');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
