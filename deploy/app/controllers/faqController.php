<?php
class Faq extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('faq');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
