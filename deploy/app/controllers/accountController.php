<?php
class Account extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('account');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
