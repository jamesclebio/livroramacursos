<?php
class Cart extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('cart');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
