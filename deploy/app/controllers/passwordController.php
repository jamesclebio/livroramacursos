<?php
class Password extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView(false);
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}

	public function reset() {
		$this->setView('password-reset');
	}

	public function remember() {
		$this->setLayout(false);
		$this->setView('password-remember');
	}
}
