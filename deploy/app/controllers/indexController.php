<?php
class Index extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('index');
		$this->setHtmlClass('layout-home');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = true;
	}
}
