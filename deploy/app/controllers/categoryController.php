<?php
class Category extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('category');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
