<?php
class Page500 extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('500');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
