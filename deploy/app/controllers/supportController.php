<?php
class Support extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('support');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}

	public function edit() {
		$this->setView('support-edit');
	}
}
