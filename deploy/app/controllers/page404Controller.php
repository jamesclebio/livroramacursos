<?php
class Page404 extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('404');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
