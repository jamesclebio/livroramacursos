<?php
class Sign extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView(false);
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}

	public function in() {
		$this->setLayout(false);
		$this->setView('sign-in');
	}

	public function up() {
		$this->setView('sign-up');
	}
}
