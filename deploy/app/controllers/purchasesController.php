<?php
class Purchases extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('purchases');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
