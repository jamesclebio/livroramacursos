<?php
class Billing extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('billing');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
