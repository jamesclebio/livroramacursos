<?php
class Courses extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('courses');
		$this->global_nav = false;
		$this->logged_nav = true;
		$this->splash = false;
	}
}
