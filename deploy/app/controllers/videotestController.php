<?php
class VideoTest extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('video-test');
		$this->global_nav = true;
		$this->logged_nav = false;
		$this->splash = false;
	}
}
