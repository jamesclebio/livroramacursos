<div class="advantages">
	<h4>Vantagens</h4>
	<div class="container">
		<div class="slider">
			<img src="<?php echo $this->_asset('default/images/advantages/clock.png'); ?>" alt="">
			<p>Cursos disponíveis 24 horas por dia</p>
		</div>
		<div class="slider">
			<img src="<?php echo $this->_asset('default/images/advantages/device.png'); ?>" alt="">
			<p>Estude sem sair de casa</p>
		</div>
		<div class="slider">
			<img src="<?php echo $this->_asset('default/images/advantages/mobile.png'); ?>" alt="">
			<p>Cursos compatíveis com tablets e dispositivos moveis</p>
		</div>
		<div class="slider">
			<img src="<?php echo $this->_asset('default/images/advantages/support.png'); ?>" alt="">
			<p>A tecnologia ao seu lado para alcançar o  sucesso profissional</p>
		</div>
		<div class="slider">
			<img src="<?php echo $this->_asset('default/images/advantages/approve.png'); ?>" alt="">
			<p>Professores especializados e com experiência em concursos públicos</p>
		</div>
	</div>
	<a href="#" class="prev">Anterior</a>
	<a href="#" class="next">Próximo</a>
</div>
