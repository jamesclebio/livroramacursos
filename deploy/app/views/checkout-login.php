<h3 class="heading-3">Acesse sua conta</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="wrapper">
	<div class="grid-content-half f-left">
		<h3 class="heading-group">Cadastre-se no <strong>Livrorama Cursos</strong></h3>
		<?php include 'advantages.php'; ?>
		<div class="wrapper m-top-15">
			<a href="#" class="link-button link-button-large link-button-submit f-right">Quero me cadastrar</a>
		</div>
	</div>

	<div class="grid-content-half f-right">
		<h3 class="heading-group">Já sou cadastrado</h3>
		<form id="form-login" method="post" action="" class="form-main">
			<fieldset>
				<legend>Login</legend>
				<label>Usuário *<input name="user" type="text" required></label>
				<label>Senha *<input name="password" type="password" required></label>
				<a href="#" data-url="<?php echo $this->_url('password/remember'); ?>" class="link-main modal-open">Esqueceu a senha?</a>
				<div class="form-action">
					<button type="submit" class="button-success button-large">Entrar</button>
				</div>
			</fieldset>
		</form>
	</div>
</div>		
</div>