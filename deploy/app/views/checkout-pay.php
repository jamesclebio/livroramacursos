<h3 class="heading-3">Quase lá...</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="wrapper">
	<div class="grid-content-half f-left">
		<h3 class="heading-group">Produto(s)</h3>
		<div class="box-well">
			<p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit</strong></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, corrupti, quam, dignissimos, maiores possimus consequuntur suscipit recusandae optio enim asperiores nulla perferendis ex laudantium cupiditate esse repellendus quos. Harum, ab.</p>
		</div>
		<div class="box-well">
			<p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit</strong></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, corrupti, quam, dignissimos, maiores possimus consequuntur suscipit recusandae optio enim asperiores nulla perferendis ex laudantium cupiditate esse repellendus quos. Harum, ab.</p>
		</div>
	</div>

	<div class="grid-content-half f-right">
		<h3 class="heading-group">Detalhes da compra</h3>
		<table class="table-buy">
			<tr>
				<th>Valor total em produtos</th>
				<td>RS 99,00</td>
			</tr>
			<tr>
				<th>Valor do desconto (cupom)</th>
				<td>RS 99,00</td>
			</tr>
			<tr class="highlight">
				<th>Total a pagar</th>
				<td data-free>Grátis</td>
			</tr>
		</table>
		<form method="post" action="url/" data-validator="<?php echo $this->_url('checkout/pay'); ?>" class="form-pay form-main">
			<fieldset>
				<legend>Pagamento</legend>
				<div class="form-action">
					<button type="submit" class="button-success button-large">Efetuar Pagamento</button>
				</div>
				<div class="wrapper m-top-30 a-right">
					<img src="<?php echo $this->_asset('default/images/logo_pagseguro.png'); ?>" alt="Pagseguro">
				</div>
			</fieldset>
		</form>
	</div>
</div>
