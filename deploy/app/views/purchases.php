<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Minhas Compras</h3>
<div class="linksbar">
	<ul>
		<li class="current"><a href="#">Todos</a></li>
		<li><a href="#">Pagos</a></li>
		<li><a href="#">Em Aberto</a></li>
		<li><a href="#">Cancelados</a></li>
	</ul>
</div>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<h4 class="heading-group">Todas as Compras</h4>

<!-- <div class="content-empty">
	<p><strong>Puxa, você ainda não realizou compras... :(</strong></p>
	<p>Para adquirir novos cursos de excelente qualidade navegue pelo site através do menu principal localizado no topo da página, escolha o(s) curso(s) de seu interesse e finalize a compra. Vamos lá, não perca mais tempo!</p>
</div> -->

<table class="table-cart">
	<thead>
		<tr>
			<th class="description">Descrição</th>
			<th class="date">Data</th>
			<th class="status">Status</th>
			<th class="price">Valor</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="description">
				<div class="item theme-course-1">
					<div class="header">
						<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
						<h5>Direito Constitucional</h5>
						<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						<div class="details">
							<div class="length">Duração <strong>16h</strong></div>
							<ul class="useful">
								<li>Expira em 12/12/1234, 12:12:12</li>
								<li><a href="<?php echo $this->_url('courses'); ?>">Assistir</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item theme-course-1">
					<div class="header">
						<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
						<h5>Direito Constitucional</h5>
						<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						<div class="details">
							<div class="length">Duração <strong>16h</strong></div>
							<ul class="useful">
								<li>Expira em 12/12/1234, 12:12:12</li>
								<li><a href="<?php echo $this->_url('courses'); ?>">Assistir</a></li>
							</ul>
						</div>
					</div>
				</div>
			</td>
			<td class="date">99/99/9999 12:15:12</td>
			<td class="status"><div class="success">Pago</div></td>
			<td class="price">R$ 99,99</td>
		</tr>
		<tr>
			<td class="description">
				<div class="item theme-course-1">
					<div class="header">
						<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
						<h5>Direito Constitucional</h5>
						<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						<div class="details">
							<div class="length">Duração <strong>16h</strong></div>
							<ul class="useful">
								<li>Expira em 12/12/1234, 12:12:12</li>
								<li><a href="<?php echo $this->_url('courses'); ?>">Assistir</a></li>
							</ul>
						</div>
					</div>
				</div>
			</td>
			<td class="date">99/99/9999 12:15:12</td>
			<td class="status">
				<div class="warning">Em Aberto</div>
				<form data-test="form1" method="post" action="ACTION-PAGE.html" data-validator="<?php echo $this->_url('checkout/pay'); ?>" class="form-pay">
					<a href="#" class="link-submit">Efetuar Pagto</a>
				</form>
			</td>
			<td class="price">R$ 99,99</td>
		</tr>
		<tr>
			<td class="description">
				<div class="item theme-course-1">
					<div class="header">
						<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
						<h5>Direito Constitucional</h5>
						<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						<div class="details">
							<div class="length">Duração <strong>16h</strong></div>
							<ul class="useful">
								<li>Expira em 12/12/1234, 12:12:12</li>
								<li><a href="<?php echo $this->_url('courses'); ?>">Assistir</a></li>
							</ul>
						</div>
					</div>
				</div>
			</td>
			<td class="date">99/99/9999 12:15:12</td>
			<td class="status">
				<div class="warning">Em Aberto</div>
				<form data-test="form2" method="post" action="ACTION-PAGE.html" data-validator="<?php echo $this->_url('checkout/pay'); ?>" class="form-pay">
					<a href="#" class="link-submit">Efetuar Pagto</a>
				</form>
			</td>
			<td class="price">R$ 99,99</td>
		</tr>
	</tbody>
</table>
