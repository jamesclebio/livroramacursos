<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Redefinição de senha</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Sua senha foi alterada com sucesso!</strong></p>
</div> -->

<!-- Form -->
<form id="form-password" method="post" action="" class="form-main m-top-30">
	<fieldset>
		<legend>Cadastro</legend>
		<div class="wrapper">
			<div class="grid-content-half f-left">
				<h4 class="heading-group">Nova Senha</h4>
				<label>Senha *<input name="senha" type="password" required></label>
				<label>Confirme a Senha *<input name="senha_conf" type="password" required></label>
			</div>
		</div>
		<div class="form-action">
			<button type="submit" class="button-submit">Confirmar</button>
		</div>
	</fieldset>
</form>
