<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Meus Cursos</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<!-- <div class="content-empty">
	<p><strong>Puxa, você ainda não possui cursos... :(</strong></p>
	<p>Para adquirir novos cursos de excelente qualidade navegue pelo site através do menu principal localizado no topo da página, escolha o(s) curso(s) de seu interesse e finalize a compra. Vamos lá, não perca mais tempo!</p>
</div> -->

<div class="viewer-courses">
	<header class="header">
		<h5>Categoria</h5>
		<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
	</header>

	<div class="videoplayer">
		<div class="container">
			<object id="video-player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="668" height="376">
				<param name="movie" value="<?php echo $this->_asset('default/movies/player/video_player8_xml.swf'); ?>">
				<param name="allowFullScreen" value="true">
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="<?php echo $this->_asset('default/movies/player/video_player8_xml.swf'); ?>" width="668" height="376" allowfullscreen="true">
				<!--<![endif]-->
				<div class="no-videoplayer">
					<h1>Ops, vídeo não suportado! :(</h1>
					<p>Aparentemente seu navegador não possui os recursos básicos para rodar nosso player de vídeo.</p>
					<p>Atualize o Flash Player e tente novamente:</p>
					<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
				</div>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
		</div>
	</div>

	<div class="video-description">
		<div class="text-body">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate, cum, aperiam, voluptatum, illo nam error obcaecati minus accusamus commodi possimus asperiores inventore nemo modi amet suscipit et nisi adipisci iusto.</p>
		</div>
	</div>

	<div class="collapse-main m-top-30">
		<a href="#" class="collapse-main-heading"><span>+</span>Fale com o professor</a>
		<div class="collapse-main-content">
			<div class="support">
				<form id="form-support" action="ajax/support.htm" class="form-main">
					<fieldset>
						<legend>Suporte</legend>
						<label>Envie uma mensagem para o professor:
							<textarea name="message" id="" cols="30" rows="10" placeholder="Digite aqui a sua dúvida" required></textarea>
						</label>
						<div class="form-action">
							<div class="alert"></div>
							<button type="submit" class="button-submit">Enviar Mensagem</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

	<div class="wrapper">
		<div class="grid-content-2 f-left">
			<h4 class="heading-group">Lista de Vídeos</h4>
			<div class="collapse-main collapse-main-open">
				<a href="#" class="collapse-main-heading"><span>+</span>Curso - Direito Trabalhista</a>
				<div class="collapse-main-content">
					<div class="alert-main alert-main-small a-right">
						<p>Este curso <strong>expira em 01 de janeiro de 2013</strong> (Restam 15 dias)</p>
					</div>

					<div class="tools-course">
						<a href="">Certificado</a>
					</div>

					<ul class="list-courses">
						<li>
							<div class="item">
								<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
								<div class="details">Professor <strong>Saulo Duarte</strong> - Aula 1, Duração <strong>14m25</strong></div>
								<div class="tags">Direito Constitucional, Informática, Medicina</div>
							</div>
							<a href="#" class="button button-view">Assistir</a>
						</li>
						<li class="highlight">
							<div class="item">
								<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
								<div class="details">Professor <strong>Saulo Duarte</strong> - Aula 1, Duração <strong>14m25</strong></div>
								<div class="tags">Direito Constitucional, Informática, Medicina</div>
							</div>
							<a href="#" class="button button-buy">Comprar</a>
						</li>
						<li>
							<div class="item">
								<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
								<div class="details">Professor <strong>Saulo Duarte</strong> - Aula 1, Duração <strong>14m25</strong></div>
								<div class="tags">Direito Constitucional, Informática, Medicina</div>
							</div>
							<a href="#" class="button button-disabled">Expirado</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="collapse-main">
				<a href="#" class="collapse-main-heading"><span>+</span>Aulas Avulsas</a>
				<div class="collapse-main-content">
					<ul class="list-courses">
						<li>
							<div class="item">
								<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
								<div class="details">Professor <strong>Saulo Duarte</strong> - Aula 1, Duração <strong>14m25</strong></div>
								<div class="tags">Direito Constitucional, Informática, Medicina</div>
							</div>
							<a href="#" class="button button-view">Assistir</a>
						</li>
						<li>
							<div class="item">
								<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
								<div class="details">Professor <strong>Saulo Duarte</strong> - Aula 1, Duração <strong>14m25</strong></div>
								<div class="tags">Direito Constitucional, Informática, Medicina</div>
							</div>
							<a href="#" class="button button-buy">Comprar</a>
						</li>
						<li>
							<div class="item">
								<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
								<div class="details">Professor <strong>Saulo Duarte</strong> - Aula 1, Duração <strong>14m25</strong></div>
								<div class="tags">Direito Constitucional, Informática, Medicina</div>
							</div>
							<a href="#" class="button button-disabled">Expirado</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="collapse-main">
				<a href="#" class="collapse-main-heading"><span>+</span>Ofertas</a>
				<div class="collapse-main-content">
					<ul class="list-courses">
						<li>
							<div class="item">
								<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
								<div class="details">Professor <strong>Saulo Duarte</strong> - Aula 1, Duração <strong>14m25</strong></div>
								<div class="tags">Direito Constitucional, Informática, Medicina</div>
							</div>
							<a href="#" class="button button-view">Assistir</a>
						</li>
						<li>
							<div class="item">
								<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
								<div class="details">Professor <strong>Saulo Duarte</strong> - Aula 1, Duração <strong>14m25</strong></div>
								<div class="tags">Direito Constitucional, Informática, Medicina</div>
							</div>
							<a href="#" class="button button-buy">Comprar</a>
						</li>
						<li>
							<div class="item">
								<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
								<div class="details">Professor <strong>Saulo Duarte</strong> - Aula 1, Duração <strong>14m25</strong></div>
								<div class="tags">Direito Constitucional, Informática, Medicina</div>
							</div>
							<a href="#" class="button button-disabled">Expirado</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="grid-content-1 f-right">
			<h4 class="heading-group">Material de Apoio</h4>
			<div class="support-links">
				<p>Lista do material de apoio relacionado ao vídeo que você está assistindo agora:</p>
				<ul>
					<li><a href="#" target="_blank"><strong>Apostila 1</strong><br>Arquivo PDF, 324KB</a></li>
					<li><a href="#" target="_blank"><strong>Apostila 2</strong><br>Arquivo PDF, 324KB</a></li>
					<li><a href="#" target="_blank"><strong>Apostila 3</strong><br>Arquivo PDF, 324KB</a></li>
					<li><a href="#" target="_blank"><strong>Apostila 4</strong><br>Arquivo PDF, 324KB</a></li>
				</ul>
			</div>

			<!-- <div class="wrapper m-top-55">
				<a href="http://www.livrorama.com.br/autor/step1.php" target="_blank"><img src="img/banners/livrorama-300x378.jpg" alt="Banner"></a>
			</div> -->
		</div>
	</div>
</div>
