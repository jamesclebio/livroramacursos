<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<!-- Resultado -->
<div class="wrapper">
	<h3 class="heading-3 heading-star-yellow">Buscando por <span>termo pesquisado</span></h3>
	<!-- <div class="content-empty">
		<p><strong>Desculpe, sua busca não retornou nenhum resultado. :(</strong></p>
		<p>Tente fazer uma nova busca ou navegue pelo site usando o menu principal localizado no topo da página.</p>
	</div> -->

	<div class="showcase-courses">
		<div class="item theme-course-1">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-2">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price free"><strong>Grátis!</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-3">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-4">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-5">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-6">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
	</div>

	<div class="pagination">
		<ul>
			<li class="current"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><span>...</span></li>
			<li><a href="#">6</a></li>
			<li><a href="#">7</a></li>
			<li><a href="#">8</a></li>
			<li><a href="#">9</a></li>
			<li><a href="#">10</a></li>
		</ul>
	</div>
</div>
