<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Acesso Restrito</h3>

<div class="content-security">
	<p><strong>Você tentou acessar uma página com conteúdo exclusivo!</strong></p>
	<p>Entre com seus dados de acesso na Área do Aluno, localizada logo acima do menu principal.</p>
	<p>Caso ainda não tenha uma conta, basta fazer um <a href="<?php echo $this->_url('sign/up'); ?>" class="link-main">novo cadastro</a>. É fácil! ;D</p>
	<p>Se mesmo depois disso o conteúdo não for exibido, por favor entre em <a href="<?php echo $this->_url('contact'); ?>" class="link-main">contato com a gente</a>!</p>
</div>
