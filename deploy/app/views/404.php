<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Ops! Não deu...</h3>

<div class="content-notfound">
	<p><strong>Não conseguimos encontrar a página que você solicitou!</strong></p>
	<p>Experimente usar nosso campo de busca mais acima ou navegue pelo site usando o menu principal localizado no topo da página.</p>
	<p>Se ainda assim não conseguir achar o que procura, <a href="<?php echo $this->_url('contact'); ?>" class="link-main">tente falar conosco</a>. Teremos prazer em ajudá-lo! ;D</p>
</div>
