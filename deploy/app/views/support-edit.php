<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Material de Apoio</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<!-- <div class="content-empty">
	<p><strong>Você ainda não possui nenhum material cadastrado para o seus vídeos.</strong></p>
	<p>Use o formulário abaixo para adicionar novos arquivos.</p>
</div> -->

<h4 class="heading-group m-top-30">Editar Arquivo</h4>
<form id="form-support" action="" class="form-main">
	<fieldset>
		<legend>Suporte</legend>
		<div class="clearfix">
			<div class="w-450 f-left">
				<label>Título *<input name="titulo" type="text" required></label>
				<label>Arquivo *
					<input name="arquivo" type="file" required>
					<div class="note">Tamanho máximo: <strong>999</strong> megabytes.</div>
					<div class="note">O arquivo selecionado irá substituir o <a href="#" target="_blank">arquivo atual</a>.</div>
				</label>
			</div>
			<div class="w-450 f-right">
				<label>Vídeos
					<select name="videos" multiple data-placeholder="Clique para selecionar os vídeos" class="chosen">
						<option value="1">Lorem ipsum dolor sit amet, consectetur sdgh ghs hsgdsd shdgsdsds adipisicing elit nost</option>
						<option value="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit nost</option>
						<option value="3">Lorem ipsum dolor sit amet, consectetur adipisicing elit nost</option>
					</select>
				</label>
			</div>
		</div>

		<div class="wrapper box-well box-well-highlight m-top-20">
			<div class="grid-content-half f-left a-right">
				<p>Digite sua <strong>senha atual</strong> no campo ao lado.</p>
				<p>Isto é necessário para comprovar a autenticidade das alterações.</p>
			</div>
			<div class="grid-content-half f-right">
				<input name="senha_atual" type="password" class="m-0" placeholder="Senha Atual" required>
			</div>
		</div>

		<div class="form-action">
			<button type="submit" class="button-submit">Confirmar Alteração</button>
		</div>
	</fieldset>
</form>
