<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Ops! Não deu...</h3>

<div class="content-server">
	<p><strong>Algo inesperado ocorreu em nosso servidor... :(</strong></p>
	<p>Aguarde alguns instantes e tente novamente.</p>
	<p>Caso o erro persista, por favor nos <a href="<?php echo $this->_url('contact'); ?>" class="link-main">envie uma mensagem</a>. Temos pressa em corrigir isso! ;D</p>
</div>
