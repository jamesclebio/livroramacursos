<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Novo Cadastro</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Seu cadastro foi feito com sucesso!</strong></p>
</div> -->

<!-- Form -->
<form id="form-signup" method="post" action="" class="form-main m-top-30">
	<fieldset>
		<legend>Cadastro</legend>
		<div class="wrapper">
			<div class="grid-content-half f-left">
				<h4 class="heading-group">Dados pessoais</h4>
				<label>Nome completo *<input name="nome" type="text" required></label>
				<label>CPF *<input name="cpf" type="text" class="mask-cpf" required></label>
				<label>RG *<input name="cpf" type="text" class="mask-cpf" required></label>
				<label>E-mail *<input name="email" type="email" required></label>
				<label>Telefone *<input name="email" type="text" class="mask-phone" required></label>
				<div class="wrapper">
					<div class="w-210 f-left">
						<label>Data de Nascimento *<input name="nascimento" type="text" class="mask-date" required></label>
					</div>
					<div class="w-210 f-right">
						<label>Sexo *
							<select name="sexo" required>
								<option value="m">Masculino</option>
								<option value="f">Feminino</option>
							</select>
						</label>
					</div>
				</div>
				<label>Nova Senha *<input name="senha" type="password" required></label>
				<label>Confirme a Nova Senha *<input name="senha_conf" type="password" required></label>
			</div>

			<div class="grid-content-half f-right">
				<h4 class="heading-group">Endereço</h4>
				<label>CEP *<input name="cep" type="text" class="mask-postal" required></label>
				<label>Endereço *<input name="endereco" type="text" required></label>
				<label>Bairro *<input name="bairro" type="text" required></label>
				<label>Número *
					<input name="numero" type="text" required>
					<div class="note">Caso não possua, digite SN.</div>
				</label>
				<label>Complemento<input name="complemento" type="text"></label>
				<label>Cidade *<input name="cidade" type="text" required></label>
				<label>Estado *
					<select name="estado" required>
						<option value="AC">Acre</option>
						<option value="AL">Alagoas</option>
						<option value="AP">Amapá</option>
						<option value="AM">Amazonas</option>
						<option value="BA">Bahia</option>
						<option value="CE">Ceará</option>
						<option value="DF">Distrito Federal</option>
						<option value="ES">Espírito Santo</option>
						<option value="GO">Goiás</option>
						<option value="MA">Maranhão</option>
						<option value="MT">Mato Grosso</option>
						<option value="MS">Mato Grosso do Sul</option>
						<option value="MG">Minas Gerais</option>
						<option value="PA">Pará</option>
						<option value="PB">Paraíba</option>
						<option value="PR">Paraná</option>
						<option value="PE">Pernambuco</option>
						<option value="PI">Piauí</option>
						<option value="RJ">Rio de janeiro</option>
						<option value="RN">Rio Grande do Norte</option>
						<option value="RS">Rio Grande do Sul</option>
						<option value="RO">Rondônia</option>
						<option value="RR">Roraima</option>
						<option value="SC">Santa Catarina</option>
						<option value="SP">São Paulo</option>
						<option value="SE">Sergipe</option>
						<option value="TO">Tocantins</option>
					</select>
				</label>
				<label class="check"><input name="newsletter" type="checkbox" value="1" checked><strong>Desejo receber por e-mail</strong> novidades do Livrorama Cursos.</label>
			</div>
		</div>

		<div class="signup-terms">
			<h4>Termo de Adesão</h4>

			<h5>1. Como funciona o Livrorama Cursos</h5>
			<p>O Livrorama Cursos oferece cursos de atualização jurídica, além de preparação para Concursos Públicos e Exame de Ordem de modo 100% on-line.</p>
			<p>A inscrição em um curso on-line deverá ocorrer no site www.livroramacursos.com.br</p>
			<p>Efetivada a inscrição, o acesso à Área do Aluno permitirá que o aluno tenha à sua disposição o curso escolhido.</p>

			<h5>2 Requisitos Mínimos</h5>
			<ul>
				<li>Conexão Internet 1MBps</li>
				<li>Navegadores recomendados: Google Chrome (29+), Mozilla Firefox (23+), Opera (12+) ou Internet Explorer (9+). Compatível com tablets e smatphones (iOS/Android/Windows Phone).</li>
				<li>Plug-In Flash Player versão 11.8 ou superior</li>
				<li>Software leitor de PDF (Recomendamos o Adobe Acrobat Reader)</li>
				<li>No Internet Explorer, ativar controle ActivexPlugin do Flash;</li>
				<li>Adobe Flash Player;</li>
				<li>Processador mínimo: Pentium III 500 Mhz;</li>
				<li>Memória RAM: mínimo 128 Mb (256Mb recomendável);</li>
				<li>Placa de Vídeo: SVGA com 8MB (16 MB recomendável);</li>
				<li>Placa de som: compatível com Microsoft® Windows® 98/2000/NT/XP;</li>
				<li>Espaço em disco (HD): 100 Mb;</li>
				<li>Tela com resolução de 1024x768 e 256 cores (mínima);</li>
			</ul>
			<p>O nosso sistema funciona em qualquer computador e conexão, desde que preencha os requisitos mínimos para execução.</p>
			<p>NÃO ACONSELHAMOS A UTILIZAÇÃO DA TECNOLOGIA 3G, UMA VEZ QUE, A TAXA DE TRANSFÊNCIA DE EXIBIÇÃO DOS VÍDEOS, DIMINUI NO DECORRER DO MÊS, DIFICULTANDO O ACESSO DO ALUNO</p>

			<h5>3. Regras aplicáveis</h5>
			<p>3.1. É expressamente vedada a gravação ou cópia das aulas on-line disponibilizadas na Área do Aluno.</p>
			<p>32. O uso da senha e o acesso ao ambiente restrito são pessoais e intransferíveis.</p>
			<p>3.3. Os vídeos ficarão disponíveis pelo período de três meses.</p>
			<p>3.4. O aluno poderá acessar 3 vezes cada vídeo</p>
			<p>3.5. O aluno é responsável pelo equipamento e acesso à internet necessários à realização das aulas (verificar requisitos técnicos mínimos).</p>
			<p>3.6.É vedado ao aluno a gravação, por qualquer meio, do conteúdo das aulas on-line. Da mesma forma, é vedado ao aluno que transfira o acesso a terceiras pessoas. O uso das aulas on-line é pessoal e intransferível.</p>

			<h5>4. Como se cadastrar</h5>
			<p>Para se cadastrar basta clicar no link Cadastre-se! localizado na barra de Área do Aluno. Em seguida é necessário preencher o formulário de cadastro e em poucos minutos você estará cadastrado.</p>

			<h5>5. Como comprar um curso</h5>
			<p>Para comprar um curso é necessário que você esteja previamente cadastrado. Comprar um curso é fácil, basta escolher o curso desejado e clicar no botão comprar. Você será direcionado para o carrinho de compras onde deverá confirmar a sua compra.</p>
			<p>Não esqueça de efetuar os testes de funcionamento para garantir sua perfeita visualização do curso.</p>

			<h5>6. Como assistir a um curso comprado.</h5>
			<p>Para assistir, acesse a "ÁREA DO ALUNO" localizado na barra superior do portal. Após digitar seu "USUÁRIO e SENHA", você deverá ir em "MINHA CONTA" e clicar em Meus Cursos.</p>
			<p>Pronto, serão exibidos todos os cursos que você comprou. Basta clicar no curso desejado.</p>


		</div>
		<label class="check"><input name="termos" type="checkbox" value="1" required><strong>Li e estou de acordo</strong> com as regras descritas no termo de adesão.</label>

		<div class="form-action">
			<button type="submit" class="button-submit">Confirmar Alteração</button>
		</div>
	</fieldset>
</form>
