<!-- Demonstração -->
<div class="wrapper">
	<h3 class="heading-3 heading-star-red">Vídeo demonstração de nossos cursos</h3>
	<div class="videoplayer-large">
		<div class="container">
			<video id="video-player-html" width="668" height="376" data-playlist="<?php echo $this->_asset('default/movies/vinheta.m4v'); ?>|<?php echo $this->_asset('default/movies/introducao.mp4'); ?>" poster="<?php echo $this->_asset('default/images/video-poster.jpg'); ?>" type="video/mp4" controls></video>
		</div>
	</div>
</div>

<!-- Cursos em destaque -->
<div class="wrapper m-bottom-30">
	<h3 class="heading-3 heading-star-yellow">Cursos em destaque</h3>
	<div class="showcase-courses">
		<div class="item theme-course-1">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-2">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price free"><strong>Grátis!</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-3">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-4">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-5">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-6">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
	</div>
</div>

<!-- Tabs Cursos -->
<div class="tabbar m-top-30">
	<ul class="tabbar-index">
		<li class="active"><a href="#tabbar-1">Reforço Universitário</a></li>
		<li><a href="#tabbar-2">OAB</a></li>
		<li><a href="#tabbar-3">Atualização Profissional</a></li>
		<li><a href="#tabbar-4">Concursos</a></li>
	</ul>

	<div class="tabbar-content" id="tabbar-1">
		<div class="showcase-courses">
			<div class="item theme-course-1">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional 1</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-2">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price free"><strong>Grátis!</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-3">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-4">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-5">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-6">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="tabbar-content" id="tabbar-2">
		<div class="showcase-courses">
			<div class="item theme-course-1">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional 2</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-2">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price free"><strong>Grátis!</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-3">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-4">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-5">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-6">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="tabbar-content" id="tabbar-3">
		<div class="showcase-courses">
			<div class="item theme-course-1">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional 3</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-2">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price free"><strong>Grátis!</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-3">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-4">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-5">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-6">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="tabbar-content" id="tabbar-4">
		<div class="showcase-courses">
			<div class="item theme-course-1">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional 4</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-2">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price free"><strong>Grátis!</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-3">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-4">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-5">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
			<div class="item theme-course-6">
				<div class="header">
					<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
					<h5>Direito Constitucional</h5>
					<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				</div>
				<div class="details">
					<div class="price">R$ <strong>699,90</strong></div>
					<div class="length">Duração <strong>16h</strong></div>
				</div>
				<ul class="action">
					<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
					<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- Banner -->
<div class="wrapper m-bottom-30">
	<a href="http://livrorama.com.br/autor/step1.php" target="_blank"><img src="<?php echo $this->_asset('default/images/banners/livroramacursos-960x92.jpg'); ?>" alt="Publique seu livro agora. É grátis!"></a>
</div>

<div class="wrapper">

	<!-- Tabs Mais vendidos / Em oferta -->
	<div class="grid-content-2 f-left">
		<div class="tabgrid">
			<ul class="tabgrid-index">
				<li class="star-green active"><a href="#tabgrid-1">Cursos mais vendidos</a></li>
				<li class="star-red"><a href="#tabgrid-2">Cursos em oferta</a></li>
			</ul>
			<div class="tabgrid-content" id="tabgrid-1">
				<div class="showcase-courses">
					<div class="item theme-course-1">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional 1</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-2">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price free"><strong>Grátis!</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-3">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-4">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-5">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-6">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="tabgrid-content" id="tabgrid-2">
				<div class="showcase-courses">
					<div class="item theme-course-1">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional 2</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-2">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price free"><strong>Grátis!</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-3">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-4">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-5">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
					<div class="item theme-course-6">
						<div class="header">
							<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
							<h5>Direito Constitucional</h5>
							<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
						</div>
						<div class="details">
							<div class="price">R$ <strong>699,90</strong></div>
							<div class="length">Duração <strong>16h</strong></div>
						</div>
						<ul class="action">
							<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
							<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="grid-content-1 f-right">

		<!-- Depoimentos -->
		<div class="testimonials">
			<h4>Depoimentos</h4>
			<blockquote>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, debitis, molestiae vitae repellat a voluptatem esse at accusamus ducimus fugit sequi voluptate pariatur dolorem ipsum voluptates. Nisi, provident ipsa non ut architecto aspernatur sed ex harum impedit fuga laboriosam minima voluptate doloremque. Rem officiis suscipit assumenda eligendi. Deleniti, libero, possimus, sapiente sint corporis magnam amet labore porro expedita qui beatae quas dolorem eos corrupti molestiae maiores est deserunt omnis suscipit necessitatibus ad non itaque nostrum cupiditate dolore.</p>
				<footer>Saulo Duarte</footer>
			</blockquote>
		</div>

		<!-- Vantagens -->
		<?php include 'advantages.php'; ?>
	</div>
</div>
