<h4 class="heading-4 heading-separate">Lembrar Senha</h4>
<form id="form-password-remember" method="post" action="ajax/password-remember.htm">
	<fieldset>
		<legend>Lembrar senha</legend>
		<input name="user" type="text" placeholder="Usuário" required>
		<button type="submit">Lembrar</button>
		<div class="alert"></div>
	</fieldset>
</form>