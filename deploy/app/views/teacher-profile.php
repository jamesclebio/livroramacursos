<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">O Professor</h3>

<!-- Sobre Curso / Autor -->
<div class="wrapper m-bottom-30">
	<div class="grid-content-1 f-left">
		<div class="author-about">
			<div class="img">
				<img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt="Avatar">
			</div>
		</div>
	</div>

	<div class="grid-content-2 f-right">
		<h4 class="heading-4">Saulo Duarte e Couto</h4>
		<div class="text-body">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, harum, enim, vel doloremque porro hic doloribus architecto vero quaerat sint sequi et neque id qui placeat fugit laborum itaque! Sint.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, praesentium nemo cupiditate odio voluptatum et dignissimos sint aliquam vero soluta id mollitia suscipit? Consequuntur, consequatur commodi temporibus nesciunt iure officiis.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos placeat ratione quam voluptate nobis quos.</p>
		</div>
	</div>
</div>

<!-- Banner -->
<div class="wrapper m-bottom-30">
	<a href="http://livrorama.com.br/autor/step1.php" target="_blank"><img src="<?php echo $this->_asset('default/images/banners/livroramacursos-960x92.jpg'); ?>" alt="Publique seu livro agora. É grátis!"></a>
</div>

<!-- Cursos do autor -->
<div class="wrapper">
	<h3 class="heading-3 heading-star-yellow">Cursos deste especialista</h3>
	<div class="showcase-courses">
		<div class="item theme-course-1">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-2">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price free"><strong>Grátis!</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-3">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-4">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-5">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
		<div class="item theme-course-6">
			<div class="header">
				<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
				<h5>Direito Constitucional</h5>
				<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
			</div>
			<div class="details">
				<div class="price">R$ <strong>699,90</strong></div>
				<div class="length">Duração <strong>16h</strong></div>
			</div>
			<ul class="action">
				<li class="more"><a href="<?php echo $this->_url('product'); ?>">+ Detalhes</a></li>
				<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
			</ul>
		</div>
	</div>
</div>
