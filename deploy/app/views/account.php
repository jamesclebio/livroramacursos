<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Meus Dados</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<!-- Form -->
<form id="form-signup" method="post" action="" class="form-main m-top-30">
	<fieldset>
		<legend>Cadastro</legend>
		<div class="wrapper">
			<div class="grid-content-half f-left">
				<h4 class="heading-group">Dados pessoais</h4>
				<label>Nome completo *<input name="nome" type="text" required readonly></label>
				<label>CPF *<input name="cpf" type="text" class="mask-cpf" required readonly></label>
				<label>RG *<input name="cpf" type="text" class="mask-cpf" required readonly></label>
				<label>E-mail *<input name="email" type="email" required readonly></label>
				<label>Telefone *<input name="email" type="text" class="mask-phone" required></label>
				<div class="wrapper">
					<div class="w-210 f-left">
						<label>Data de Nascimento *<input name="nascimento" type="text" class="mask-date" required></label>
					</div>
					<div class="w-210 f-right">
						<label>Sexo *
							<select name="sexo" required>
								<option value="m">Masculino</option>
								<option value="f">Feminino</option>
							</select>
						</label>
					</div>
				</div>
				<label>Nova Senha *<input name="senha" type="password" required></label>
				<label>Confirme a Nova Senha *<input name="senha_conf" type="password" required></label>
			</div>

			<div class="grid-content-half f-right">
				<h4 class="heading-group">Endereço</h4>
				<label>CEP *<input name="cep" type="text" class="mask-postal" required></label>
				<label>Endereço *<input name="endereco" type="text" required></label>
				<label>Bairro *<input name="bairro" type="text" required></label>
				<label>Número *
					<input name="numero" type="text" required>
					<div class="note">Caso não possua, digite SN.</div>
				</label>
				<label>Complemento<input name="complemento" type="text"></label>
				<label>Cidade *<input name="cidade" type="text" required></label>
				<label>Estado *
					<select name="estado" required>
						<option value="AC">Acre</option>
						<option value="AL">Alagoas</option>
						<option value="AP">Amapá</option>
						<option value="AM">Amazonas</option>
						<option value="BA">Bahia</option>
						<option value="CE">Ceará</option>
						<option value="DF">Distrito Federal</option>
						<option value="ES">Espírito Santo</option>
						<option value="GO">Goiás</option>
						<option value="MA">Maranhão</option>
						<option value="MT">Mato Grosso</option>
						<option value="MS">Mato Grosso do Sul</option>
						<option value="MG">Minas Gerais</option>
						<option value="PA">Pará</option>
						<option value="PB">Paraíba</option>
						<option value="PR">Paraná</option>
						<option value="PE">Pernambuco</option>
						<option value="PI">Piauí</option>
						<option value="RJ">Rio de janeiro</option>
						<option value="RN">Rio Grande do Norte</option>
						<option value="RS">Rio Grande do Sul</option>
						<option value="RO">Rondônia</option>
						<option value="RR">Roraima</option>
						<option value="SC">Santa Catarina</option>
						<option value="SP">São Paulo</option>
						<option value="SE">Sergipe</option>
						<option value="TO">Tocantins</option>
					</select>
				</label>
			</div>
		</div>

		<div class="wrapper group-separate">
			<div class="grid-content-half f-left">
				<h4 class="heading-group">Perfil</h4>
				<label>Sobre Você *<textarea name="sobre" cols="30" rows="10" class="h-180" required></textarea></label>
				<label>Alterar Imagem
					<input name="imagem" type="file">
					<div class="note">Formato: <strong>JPG</strong> ou <strong>PNG</strong>. Dimensões mínimas: <strong>999 x 999</strong> pixels.</div>
					<div class="note">A imagem selecionada irá substituir <a href="#" target="_blank">sua imagem de perfil atual</a>.</div>
				</label>
			</div>

			<div class="grid-content-half f-right">
				<h4 class="heading-group">Dados Bancários</h4>
				<label>Banco *
					<select id="banco" name="banco" required>
						<option value="246">246 - Banco ABC Brasil S.A.</option>
						<option value="025">025 - Banco Alfa S.A.</option>
						<option value="641">641 - Banco Alvorada S.A.</option>
						<option value="029">029 - Banco Banerj S.A.</option>
						<option value="000">000 - Banco Bankpar S.A.</option>
						<option value="740">740 - Banco Barclays S.A.</option>
						<option value="107">107 - Banco BBM S.A.</option>
						<option value="031">031 - Banco Beg S.A.</option>
						<option value="739">739 - Banco BGN S.A.</option>
						<option value="096">096 - Banco BM&F de Serviços de Liquidação e Custódia S.A</option>
						<option value="318">318 - Banco BMG S.A.</option>
						<option value="752">752 - Banco BNP Paribas Brasil S.A.</option>
						<option value="248">248 - Banco Boavista Interatlântico S.A.</option>
						<option value="218">218 - Banco Bonsucesso S.A.</option>
						<option value="065">065 - Banco Bracce S.A.</option>
						<option value="036">036 - Banco Bradesco BBI S.A.</option>
						<option value="204">204 - Banco Bradesco Cartões S.A.</option>
						<option value="237">237 - Banco Bradesco S.A.</option>
						<option value="225">225 - Banco Brascan S.A.</option>
						<option value="208">208 - Banco BTG Pactual S.A.</option>
						<option value="044">044 - Banco BVA S.A.</option>
						<option value="263">263 - Banco Cacique S.A.</option>
						<option value="473">473 - Banco Caixa Geral - Brasil S.A.</option>
						<option value="040">040 - Banco Cargill S.A.</option>
						<option value="233">233 - Banco Cifra S.A.</option>
						<option value="745">745 - Banco Citibank S.A.</option>
						<option value="215">215 - Banco Comercial e de Investimento Sudameris S.A.</option>
						<option value="756">756 - Banco Cooperativo do Brasil S.A. - BANCOOB</option>
						<option value="748">748 - Banco Cooperativo Sicredi S.A.</option>
						<option value="222">222 - Banco Credit Agricole Brasil S.A.</option>
						<option value="505">505 - Banco Credit Suisse (Brasil) S.A.</option>
						<option value="229">229 - Banco Cruzeiro do Sul S.A.</option>
						<option value="003">003 - Banco da Amazônia S.A.</option>
						<option value="083">083 - Banco da China Brasil S.A.</option>
						<option value="707">707 - Banco Daycoval S.A.</option>
						<option value="024">024 - Banco de Pernambuco S.A. - BANDEPE</option>
						<option value="456">456 - Banco de Tokyo-Mitsubishi UFJ Brasil S.A.</option>
						<option value="214">214 - Banco Dibens S.A.</option>
						<option value="001">001 - Banco do Brasil S.A.</option>
						<option value="047">047 - Banco do Estado de Sergipe S.A.</option>
						<option value="037">037 - Banco do Estado do Pará S.A.</option>
						<option value="041">041 - Banco do Estado do Rio Grande do Sul S.A.</option>
						<option value="004">004 - Banco do Nordeste do Brasil S.A.</option>
						<option value="265">265 - Banco Fator S.A.</option>
						<option value="224">224 - Banco Fibra S.A.</option>
						<option value="626">626 - Banco Ficsa S.A.</option>
						<option value="394">394 - Banco Finasa BMC S.A.</option>
						<option value="612">612 - Banco Guanabara S.A.</option>
						<option value="063">063 - Banco Ibi S.A. Banco Múltiplo</option>
						<option value="604">604 - Banco Industrial do Brasil S.A.</option>
						<option value="320">320 - Banco Industrial e Comercial S.A.</option>
						<option value="653">653 - Banco Indusval S.A.</option>
						<option value="249">249 - Banco Investcred Unibanco S.A.</option>
						<option value="184">184 - Banco Itaú BBA S.A.</option>
						<option value="479">479 - Banco ItaúBank S.A</option>
						<option value="376">376 - Banco J. P. Morgan S.A.</option>
						<option value="074">074 - Banco J. Safra S.A.</option>
						<option value="217">217 - Banco John Deere S.A.</option>
						<option value="600">600 - Banco Luso Brasileiro S.A.</option>
						<option value="389">389 - Banco Mercantil do Brasil S.A.</option>
						<option value="746">746 - Banco Modal S.A.</option>
						<option value="045">045 - Banco Opportunity S.A.</option>
						<option value="079">079 - Banco Original do Agronegócio S.A.</option>
						<option value="623">623 - Banco Panamericano S.A.</option>
						<option value="611">611 - Banco Paulista S.A.</option>
						<option value="643">643 - Banco Pine S.A.</option>
						<option value="638">638 - Banco Prosper S.A.</option>
						<option value="747">747 - Banco Rabobank International Brasil S.A.</option>
						<option value="356">356 - Banco Real S.A.</option>
						<option value="633">633 - Banco Rendimento S.A.</option>
						<option value="072">072 - Banco Rural Mais S.A.</option>
						<option value="453">453 - Banco Rural S.A.</option>
						<option value="422">422 - Banco Safra S.A.</option>
						<option value="033">033 - Banco Santander (Brasil) S.A.</option>
						<option value="250">250 - Banco Schahin S.A.</option>
						<option value="749">749 - Banco Simples S.A.</option>
						<option value="366">366 - Banco Société Générale Brasil S.A.</option>
						<option value="637">637 - Banco Sofisa S.A.</option>
						<option value="012">012 - Banco Standard de Investimentos S.A.</option>
						<option value="464">464 - Banco Sumitomo Mitsui Brasileiro S.A.</option>
						<option value="634">634 - Banco Triângulo S.A.</option>
						<option value="655">655 - Banco Votorantim S.A.</option>
						<option value="610">610 - Banco VR S.A.</option>
						<option value="119">119 - Banco Western Union do Brasil S.A.</option>
						<option value="370">370 - Banco WestLB do Brasil S.A.</option>
						<option value="021">021 - BANESTES S.A. Banco do Estado do Espírito Santo</option>
						<option value="719">719 - Banif-Banco Internacional do Funchal (Brasil)S.A.</option>
						<option value="755">755 - Bank of America Merrill Lynch Banco Múltiplo S.A.</option>
						<option value="073">073 - BB Banco Popular do Brasil S.A.</option>
						<option value="078">078 - BES Investimento do Brasil S.A.-Banco de Investimento</option>
						<option value="069">069 - BPN Brasil Banco Múltiplo S.A.</option>
						<option value="070">070 - BRB - Banco de Brasília S.A.</option>
						<option value="104">104 - Caixa Econômica Federal</option>
						<option value="477">477 - Citibank S.A.</option>
						<option value="487">487 - Deutsche Bank S.A. - Banco Alemão</option>
						<option value="064">064 - Goldman Sachs do Brasil Banco Múltiplo S.A.</option>
						<option value="062">062 - Hipercard Banco Múltiplo S.A.</option>
						<option value="399">399 - HSBC Bank Brasil S.A. - Banco Múltiplo</option>
						<option value="492">492 - ING Bank N.V.</option>
						<option value="652">652 - Itaú Unibanco Holding S.A.</option>
						<option value="341">341 - Itaú Unibanco S.A.</option>
						<option value="488">488 - JPMorgan Chase Bank</option>
						<option value="751">751 - Scotiabank Brasil S.A. Banco Múltiplo</option>
						<option value="409">409 - UNIBANCO - União de Bancos Brasileiros S.A.</option>
						<option value="230">230 - Unicard Banco Múltiplo S.A. </option>
					</select>
				</label>
				<label>Tipo da Conta *
					<select name="tipoconta">
						<option value="C">Conta Corrente</option>
						<option value="P">Poupança</option>
					</select>
				</label>
				<div class="wrapper">
					<div class="w-150 f-left">
						<label>Número da Agência *<input name="agencia" type="text" required></label>
					</div>
					<div class="w-270 f-right">
						<label>Número da Conta *<input name="conta" type="text" required></label>
					</div>
				</div>
				<label>Titular da Conta *<input name="titular" type="text" required readonly></label>
			</div>
		</div>

		<div class="wrapper box-well box-well-highlight m-top-20">
			<div class="grid-content-half f-left a-right">
				<p>Digite sua <strong>senha atual</strong> no campo ao lado.</p>
				<p>Isto é necessário para comprovar a autenticidade das alterações.</p>
			</div>
			<div class="grid-content-half f-right">
				<input name="senha_atual" type="password" class="m-0" placeholder="Senha Atual" required>
			</div>
		</div>

		<div class="form-action">
			<button type="submit" class="button-submit">Confirmar Alteração</button>
		</div>
	</fieldset>
</form>
