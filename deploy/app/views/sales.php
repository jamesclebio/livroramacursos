<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Minhas Vendas</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<!-- <div class="content-empty">
	<p><strong>Nenhuma venda referente aos seus vídeos foi realizada até o momento.</strong></p>
</div> -->

<table class="table-cart">
	<thead>
		<tr>
			<th class="date">Data</th>
			<th class="description">Curso</th>
			<th class="list">Vídeo(s)</th>
			<th class="price">Valor/Vídeo</th>
			<th class="price">Valor</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="date">99/99/9999</td>
			<td class="description"><strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong></td>
			<td class="list">
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, doloribus natus commodi ab possimus alias!</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad doloribus similique enim provident harum?</li>
				</ul>
			</td>
			<td class="price">R$ 22,22</td>
			<td class="price">R$ 99,99</td>
		</tr>
		<tr>
			<td class="date">99/99/9999</td>
			<td class="description"><strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong></td>
			<td class="list">
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, doloribus natus commodi ab possimus alias!</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad doloribus similique enim provident harum?</li>
				</ul>
			</td>
			<td class="price">R$ 22,22</td>
			<td class="price">R$ 99,99</td>
		</tr>
		<tr>
			<td class="date">99/99/9999</td>
			<td class="description"><strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong></td>
			<td class="list">
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, doloribus natus commodi ab possimus alias!</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad doloribus similique enim provident harum?</li>
				</ul>
			</td>
			<td class="price">R$ 22,22</td>
			<td class="price">R$ 99,99</td>
		</tr>
	</tbody>
</table>
<div class="totalbar">Valor total recebido: <strong>R$ 999,99</strong></div>
<div class="totalbar totalbar-highlight">Valor total a receber: <strong>R$ 999,99</strong></div>
