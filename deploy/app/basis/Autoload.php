<?php
/**
 * Basis
 *
 * @version 0.1
 * @author James Clébio <jamesclebio@gmail.com>
 * @link http://jamesclebio.github.io/basis/
 * @license http://jamesclebio.mit-license.org/
 */

class Autoload
{
	static public function core($class) {
		$file = CORE . $class . '.php';

		if (file_exists($file))
			require_once($file);
	}

	static public function model($class) {
		$file = MODELS . $class . '.php';

		if (file_exists($file))
			require_once($file);
	}

	static public function helper($class) {
		$file = HELPERS . $class . '.php';

		if (file_exists($file))
			require_once($file);
	}
}
