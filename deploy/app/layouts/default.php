<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js layout-piodecimo" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Livrorama Cursos</title>
	<link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
	<meta name="description" content="Livrorama Cursos - Melhore sua vida. Conquiste seus objetivos.">
	<meta name="keywords" content="livrorama, cursos, cursos online, direito, oab, concurso">
	<meta property="og:title" content="Livrorama Cursos - Melhore sua vida. Conquiste seus objetivos.">
	<meta property="og:url" content="http://www.livroramacursos.com.br">
	<meta property="og:image" content="http://www.livroramacursos.com.br/static/site/img/og_livroramacursos.jpg">
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<!-- LIVRORAMA TOP -->
	<div id="livrorama-top">
		<div class="container">
			<h3><a href="http://www.livrorama.com.br" target="_blank">Livrorama</a></h3>
			<ul>
				<li><a href="http://www.livrorama.com.br/loja/signup.php" target="_blank">Cadastre-se!</a></li>
				<li><a href="http://www.livrorama.com.br/autor/step1.php" target="_blank">Publique<br><strong>seu livro</strong></a></li>
				<li><a href="http://www.livrorama.com.br/loja/faq.php" target="_blank">Dúvidas</a></li>
				<li><a href="http://www.livrorama.com.br/autor/dashboard.php" target="_blank">Portal<br><strong>do Autor</strong></a></li>
			</ul>
		</div>
	</div>

	<!-- GLOBAL TRAY -->
	<div class="global-tray">
		<div class="container">
			<!-- <h1><a href="<?php echo $this->_url('root'); ?>">Livrorama Cursos</a></h1> -->

			<h1><a href="<?php echo $this->_url('root'); ?>">Faculdade Pio Décimo</a></h1>
			<h2><a href="<?php echo $this->_url('root'); ?>">Livrorama Cursos</a></h2>

			<nav class="nav">
				<ul>
					<li><a href="<?php echo $this->_url('teacher/all'); ?>">Nossos Professores</a></li>
					<li><a href="<?php echo $this->_url('faq'); ?>">Dúvidas</a></li>
					<li><a href="<?php echo $this->_url('contact'); ?>">Fale Conosco</a></li>
				</ul>
			</nav>

			<a href="<?php echo $this->_url('cart'); ?>" class="cart">
				<div class="label">Meu Carrinho</div>
				<div class="total">299 cursos</div>
			</a>
		</div>
	</div>

	<!-- GLOBAL HEADER -->
	<header class="global-header">
		<div class="slider">
			<img src="<?php echo $this->_asset('default/images/home-slider/homem.jpg'); ?>" alt="">
			<img src="<?php echo $this->_asset('default/images/home-slider/mulher.jpg'); ?>" alt="">
			<img src="<?php echo $this->_asset('default/images/home-slider/garoto.jpg'); ?>" alt="">
			<img src="<?php echo $this->_asset('default/images/home-slider/casal.jpg'); ?>" alt="">
		</div>

		<div class="login">
			<!-- <h4>Olá, James Clébio!</h4>
			<div class="logged">
				<ul>
					<li class="account"><a href="#">Minha Conta</a>
						<ul>
							<li><a href="<?php echo $this->_url('courses'); ?>">Meus Cursos</a></li>
							<li><a href="<?php echo $this->_url('support'); ?>">Material de Suporte</a></li>
							<li><a href="<?php echo $this->_url('purchases'); ?>">Minhas Compras</a></li>
							<li><a href="<?php echo $this->_url('sales'); ?>">Minhas Vendas</a></li>
							<li><a href="<?php echo $this->_url('billing'); ?>">Meu Extrato</a></li>
							<li><a href="<?php echo $this->_url('account'); ?>">Meus Dados</a></li>
						</ul>
					</li>
					<li class="logout"><a href="#">Sair</a></li>
				</ul>
				<div class="lastaccess">Seu último acesso foi em 99/99/9999, às 99:99:99</div>
			</div> -->

			<h4>Área do Aluno<span class="arrow"></span></h4>
			<form id="form-top-login" method="post" action="<?php echo $this->_url('sign/in'); ?>">
				<fieldset>
					<legend>Login</legend>
					<input name="user" type="text" placeholder="Usuário" required>
					<input name="password" type="password" placeholder="Senha" required>
					<button type="submit">Entrar</button>
					<div class="util">
						<div class="alert"></div>
						<ul class="links">
							<li><a href="#" data-url="<?php echo $this->_url('password/remember'); ?>" class="modal-open">Esqueceu a senha?</a></li>
							<li><a href="<?php echo $this->_url('sign/up'); ?>"><strong>Cadastre-se!</strong></a></li>
						</ul>
					</div>
				</fieldset>
			</form>
		</div>
	</header>

	<?php if ($this->global_nav): ?>
		<!-- GLOBAL NAV -->
		<nav class="global-nav">
			<h4>Cursos<br>On-Line</h4>
			<ul>
				<li><a href="#"><span>Reforço Universitário</span></a>
					<ul>
						<li><a href="#">Reforço Universitário (1)</a></li>
						<li><a href="#">Reforço Universitário (2)</a></li>
						<li><a href="#">Reforço Universitário (3)</a></li>
						<li><a href="#">Reforço Universitário (4)</a></li>
						<li><a href="#">Reforço Universitário (5)</a></li>
						<li><a href="#">Reforço Universitário (6)</a></li>
						<li><a href="#">Reforço Universitário (7)</a></li>
						<li><a href="#">Reforço Universitário (8)</a></li>
						<li><a href="#">Reforço Universitário (9)</a></li>
						<li><a href="#">Reforço Universitário (10)</a></li>
						<li><a href="#">Reforço Universitário (11)</a></li>
						<li><a href="#">Reforço Universitário (12)</a></li>
					</ul>
				</li>
				<li><a href="#"><span>OAB</span></a>
					<ul>
						<li><a href="#">Exame de Ordem (1)</a></li>
						<li><a href="#">Exame de Ordem (2)</a></li>
						<li><a href="#">Exame de Ordem (3)</a></li>
						<li><a href="#">Exame de Ordem (4)</a></li>
						<li><a href="#">Exame de Ordem (5)</a></li>
						<li><a href="#">Exame de Ordem (6)</a></li>
						<li><a href="#">Exame de Ordem (7)</a></li>
						<li><a href="#">Exame de Ordem (8)</a></li>
						<li><a href="#">Exame de Ordem (9)</a></li>
						<li><a href="#">Exame de Ordem (10)</a></li>
						<li><a href="#">Exame de Ordem (11)</a></li>
						<li><a href="#">Exame de Ordem (12)</a></li>
					</ul>
				</li>
				<li><a href="#"><span>Atualização Profissional</span></a>
					<ul>
						<li><a href="#">Atualização Profissional (1)</a></li>
						<li><a href="#">Atualização Profissional (2)</a></li>
						<li><a href="#">Atualização Profissional (3)</a></li>
						<li><a href="#">Atualização Profissional (4)</a></li>
						<li><a href="#">Atualização Profissional (5)</a></li>
						<li><a href="#">Atualização Profissional (6)</a></li>
						<li><a href="#">Atualização Profissional (7)</a></li>
						<li><a href="#">Atualização Profissional (8)</a></li>
						<li><a href="#">Atualização Profissional (9)</a></li>
						<li><a href="#">Atualização Profissional (10)</a></li>
						<li><a href="#">Atualização Profissional (11)</a></li>
						<li><a href="#">Atualização Profissional (12)</a></li>
					</ul>
				</li>
				<li><a href="#"><span>Nossos Professores</span></a></li>
				<li><a href="#"><span>Dúvidas</span></a></li>
			</ul>
		</nav>
	<?php endif; ?>

	<?php if ($this->logged_nav): ?>
		<!-- NAV LOGGED -->
		<nav class="nav-logged">
			<ul>
				<li><a href="<?php echo $this->_url('courses'); ?>">Meus Cursos</a></li>
				<li><a href="<?php echo $this->_url('support'); ?>">Material de Suporte</a></li>
				<li><a href="<?php echo $this->_url('purchases'); ?>">Minhas Compras</a></li>
				<li><a href="<?php echo $this->_url('sales'); ?>">Minhas Vendas</a></li>
				<li><a href="<?php echo $this->_url('billing'); ?>">Meu Extrato</a></li>
				<li><a href="<?php echo $this->_url('account'); ?>">Meus Dados</a></li>
			</ul>
		</nav>
	<?php endif; ?>

	<!-- GLOBAL SEARCH -->
	<div class="global-search">
		<div class="search">
			<form id="form-top-search" method="get" action="">
				<fieldset>
					<legend>Pesquisa</legend>
					<input name="q" type="text" placeholder="Digite aqui o que você está procurando">
					<button type="submit">Pesquisar</button>
				</fieldset>
			</form>
		</div>
	</div>

	<!-- GLOBAL CONTENT -->
	<div class="global-content">
		<?php $this->getView(); ?>
	</div>

	<!-- GLOBAL FOOTER -->
	<footer class="global-footer">
		<div class="site-shorts">
			<div class="container">
				<div class="item item-highlight">
					<h4><span>Livrorama Cursos</span></h4>
					<ul>
						<li><a href="<?php echo $this->_url('courses'); ?>">Meus Cursos</a></li>
						<li><a href="<?php echo $this->_url('support'); ?>">Material de Suporte</a></li>
						<li><a href="<?php echo $this->_url('purchases'); ?>">Minhas Compras</a></li>
						<li><a href="<?php echo $this->_url('sales'); ?>">Minhas Vendas</a></li>
						<li><a href="<?php echo $this->_url('billing'); ?>">Meu Extrato</a></li>
						<li><a href="<?php echo $this->_url('account'); ?>">Meus Dados</a></li>
						<li><a href="<?php echo $this->_url('teacher/all'); ?>">Nossos Professores</a></li>
						<li><a href="<?php echo $this->_url('faq'); ?>">Dúvidas</a></li>
						<li><a href="<?php echo $this->_url('contact'); ?>">Fale Conosco</a></li>
					</ul>
				</div>
				<div class="item">
					<h4><span>Atualização Profissional</span></h4>
					<ul>
						<li><a href="#">Atualização Profissional (1)</a></li>
						<li><a href="#">Atualização Profissional (2)</a></li>
						<li><a href="#">Atualização Profissional (3)</a></li>
						<li><a href="#">Atualização Profissional (4)</a></li>
						<li><a href="#">Atualização Profissional (5)</a></li>
						<li><a href="#">Atualização Profissional (6)</a></li>
						<li><a href="#">Atualização Profissional (7)</a></li>
						<li><a href="#">Atualização Profissional (8)</a></li>
						<li><a href="#">Atualização Profissional (9)</a></li>
						<li><a href="#">Atualização Profissional (10)</a></li>
						<li><a href="#">Atualização Profissional (11)</a></li>
						<li><a href="#">Atualização Profissional (12)</a></li>
					</ul>
				</div>
				<div class="item">
					<h4><span>Mini Cursos</span></h4>
					<ul>
						<li><a href="#">Mini Curso (1)</a></li>
						<li><a href="#">Mini Curso (2)</a></li>
						<li><a href="#">Mini Curso (3)</a></li>
						<li><a href="#">Mini Curso (4)</a></li>
						<li><a href="#">Mini Curso (5)</a></li>
						<li><a href="#">Mini Curso (6)</a></li>
						<li><a href="#">Mini Curso (7)</a></li>
						<li><a href="#">Mini Curso (8)</a></li>
						<li><a href="#">Mini Curso (9)</a></li>
						<li><a href="#">Mini Curso (10)</a></li>
						<li><a href="#">Mini Curso (11)</a></li>
						<li><a href="#">Mini Curso (12)</a></li>
					</ul>
				</div>
				<div class="item">
					<h4><span>Concursos Públicos</span></h4>
					<ul>
						<li><a href="#">Concurso Público (1)</a></li>
						<li><a href="#">Concurso Público (2)</a></li>
						<li><a href="#">Concurso Público (3)</a></li>
						<li><a href="#">Concurso Público (4)</a></li>
						<li><a href="#">Concurso Público (5)</a></li>
						<li><a href="#">Concurso Público (6)</a></li>
						<li><a href="#">Concurso Público (7)</a></li>
						<li><a href="#">Concurso Público (8)</a></li>
						<li><a href="#">Concurso Público (9)</a></li>
						<li><a href="#">Concurso Público (10)</a></li>
						<li><a href="#">Concurso Público (11)</a></li>
						<li><a href="#">Concurso Público (12)</a></li>
					</ul>
				</div>
				<div class="item">
					<h4><span>Reforço Universitário</span></h4>
					<ul>
						<li><a href="#">Reforço Universitário (1)</a></li>
						<li><a href="#">Reforço Universitário (2)</a></li>
						<li><a href="#">Reforço Universitário (3)</a></li>
						<li><a href="#">Reforço Universitário (4)</a></li>
						<li><a href="#">Reforço Universitário (5)</a></li>
						<li><a href="#">Reforço Universitário (6)</a></li>
						<li><a href="#">Reforço Universitário (7)</a></li>
						<li><a href="#">Reforço Universitário (8)</a></li>
						<li><a href="#">Reforço Universitário (9)</a></li>
						<li><a href="#">Reforço Universitário (10)</a></li>
						<li><a href="#">Reforço Universitário (11)</a></li>
						<li><a href="#">Reforço Universitário (12)</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="payments">
			<div class="container">
				<img src="<?php echo $this->_asset('default/images/payments.png'); ?>" alt="Formas de pagamento" title="Este site aceita pagamentos com Visa, MasterCard, Diners, American Express, Hipercard, Aura, Elo, PLENOCard, PersonalCard, BrasilCard, FORTBRASIL, Bradesco, Itaú, Banco do Brasil, Banrisul, Banco HSBC, Oi Paggo, saldo em conta PagSeguro, boleto e PinCode PagSeguro">
				<a href="https://pagseguro.uol.com.br/para_voce/protecao_contra_fraudes.jhtml#rmcl" target="_blank" class="pagseguro"><img src="<?php echo $this->_asset('default/images/pagseguro.png'); ?>" alt="PagSeguro" title="Este site usa PagSeguro"></a>
			</div>
		</div>

		<div class="copyright">
			<a href="<?php $this->_url('root'); ?>" class="livroramacursos">©2013 - Todos os direitos reservados</a>
			<div class="social">
				<div class="services"></div>
			</div>
			<a href="http://www.agw.com.br" target="_blank" class="agw">AGW Internet</a>
		</div>
	</footer>

	<?php if ($this->splash): ?>
	<!-- SPLASH -->
	<div class="splash-main splash-main-type-1" data-blank="true" data-timeout="7000" data-close="true">
		<div class="content">
			<a href="https://www.facebook.com/photo.php?fbid=481832031915426&set=a.303677843064180.62062.142271742538125&type=1&theater"><img src="<?php echo $this->_asset('default/images/splashes/promocao-compartilhe-e-ganhe.jpg'); ?>" alt="Promoção Compartilhe e Ganhe"></a>
		</div>
	</div>
	<?php endif; ?>

	<script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>
