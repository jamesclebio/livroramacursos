<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js layout-buy" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Livrorama Cursos</title>
	<link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<!-- GLOBAL TRAY -->
	<div class="global-tray">
		<div class="container">
			<h1><a href="<?php echo $this->_url('root'); ?>">Livrorama Cursos</a></h1>

			<a href="<?php echo $this->_url('cart'); ?>" class="cart">
				<div class="label">Meu Carrinho</div>
				<div class="total">299 cursos</div>
			</a>
		</div>
	</div>

	<!-- GLOBAL HEADER -->
	<header class="global-header">
		<div class="buy-steps">
			<div class="step step-highlight">Identificação <span>1</span></div>
			<div class="step <?php echo ($this->_get('step') >= 2 ? 'step-highlight' : '') ?>">Pagamento <span>2</span></div>
			<div class="step <?php echo ($this->_get('step') >= 3 ? 'step-highlight' : '') ?>">Confirmação <span>3</span></div>
		</div>
	</header>

	<?php if ($this->nav_global): ?>
		<!-- GLOBAL NAV -->
		<nav class="global-nav">
			<h4>Cursos<br>On-Line</h4>
			<ul>
				<li><a href="#"><span>Reforço Universitário</span></a>
					<ul>
						<li><a href="#">Reforço Universitário (1)</a></li>
						<li><a href="#">Reforço Universitário (2)</a></li>
						<li><a href="#">Reforço Universitário (3)</a></li>
						<li><a href="#">Reforço Universitário (4)</a></li>
						<li><a href="#">Reforço Universitário (5)</a></li>
						<li><a href="#">Reforço Universitário (6)</a></li>
						<li><a href="#">Reforço Universitário (7)</a></li>
						<li><a href="#">Reforço Universitário (8)</a></li>
						<li><a href="#">Reforço Universitário (9)</a></li>
						<li><a href="#">Reforço Universitário (10)</a></li>
						<li><a href="#">Reforço Universitário (11)</a></li>
						<li><a href="#">Reforço Universitário (12)</a></li>
					</ul>
				</li>
				<li><a href="#"><span>OAB</span></a>
					<ul>
						<li><a href="#">Exame de Ordem (1)</a></li>
						<li><a href="#">Exame de Ordem (2)</a></li>
						<li><a href="#">Exame de Ordem (3)</a></li>
						<li><a href="#">Exame de Ordem (4)</a></li>
						<li><a href="#">Exame de Ordem (5)</a></li>
						<li><a href="#">Exame de Ordem (6)</a></li>
						<li><a href="#">Exame de Ordem (7)</a></li>
						<li><a href="#">Exame de Ordem (8)</a></li>
						<li><a href="#">Exame de Ordem (9)</a></li>
						<li><a href="#">Exame de Ordem (10)</a></li>
						<li><a href="#">Exame de Ordem (11)</a></li>
						<li><a href="#">Exame de Ordem (12)</a></li>
					</ul>
				</li>
				<li><a href="#"><span>Atualização Profissional</span></a>
					<ul>
						<li><a href="#">Atualização Profissional (1)</a></li>
						<li><a href="#">Atualização Profissional (2)</a></li>
						<li><a href="#">Atualização Profissional (3)</a></li>
						<li><a href="#">Atualização Profissional (4)</a></li>
						<li><a href="#">Atualização Profissional (5)</a></li>
						<li><a href="#">Atualização Profissional (6)</a></li>
						<li><a href="#">Atualização Profissional (7)</a></li>
						<li><a href="#">Atualização Profissional (8)</a></li>
						<li><a href="#">Atualização Profissional (9)</a></li>
						<li><a href="#">Atualização Profissional (10)</a></li>
						<li><a href="#">Atualização Profissional (11)</a></li>
						<li><a href="#">Atualização Profissional (12)</a></li>
					</ul>
				</li>
				<li><a href="#"><span>Nossos Professores</span></a></li>
				<li><a href="#"><span>Dúvidas</span></a></li>
			</ul>
		</nav>
	<?php endif; ?>

	<?php if ($this->nav_logged): ?>
		<!-- NAV LOGGED -->
		<nav class="nav-logged">
			<ul>
				<li><a href="<?php echo $this->_url('courses'); ?>">Meus Cursos</a></li>
				<li><a href="<?php echo $this->_url('support'); ?>">Material de Suporte</a></li>
				<li><a href="<?php echo $this->_url('purchases'); ?>">Minhas Compras</a></li>
				<li><a href="<?php echo $this->_url('sales'); ?>">Minhas Vendas</a></li>
				<li><a href="<?php echo $this->_url('billings'); ?>">Meu Extrato</a></li>
				<li><a href="<?php echo $this->_url('account'); ?>">Meus Dados</a></li>
			</ul>
		</nav>
	<?php endif; ?>

	<!-- GLOBAL SEARCH -->
	<div class="global-search">
		<div class="search">
			<form id="form-top-search" method="get" action="">
				<fieldset>
					<legend>Pesquisa</legend>
					<input name="q" type="text" placeholder="Digite aqui o que você está procurando">
					<button type="submit">Pesquisar</button>
				</fieldset>
			</form>
		</div>
	</div>

	<!-- GLOBAL CONTENT -->
	<div class="global-content">
		<?php $this->getView(); ?>
	</div>

	<script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>
